const express = require(`express`)
const router = express.Router();


// controllers
const courseController = require('./../controllers/courseControllers.js')
const auth = require("./../auth")
	
// retrieve active courses
router.get('/active', (req, res) =>{

	courseController.activeCourses().then(result => res.send(result));
})


// retrieve all courses
router.get('/all', (req, res) =>{

	courseController.getAllCourses().then(result => res.send(result));
})


// add course
// create a route to add course and return true if course is successfully saved, else return false if not.
router.post('/addCourse', (req,res) =>{
	// console.log(req.body)

	courseController.addCourse(req.body).then(result => res.send(result))
})

// make a route to get single course
router.get(`/:courseId`, auth.verify, (req,res)=>{

		courseController.getSingleCourse(req.params).then(result => res.send(result))
})
// update course
router.put(`/:courseId/edit`, auth.verify, (req,res)=>{
	// console.log(req.params.courseId)

	courseController.editCourse(req.params.courseId, req.body).then(result => res.send(result))
})

// make a route to archive course
	// meaning update status from true to false

router.put(`/:courseId/archive`, auth.verify, (req,res) => {

	courseController.archiveCourse(req.params.courseId).then(result => res.send(result))
})

// make a route to unarchive
router.put(`/:courseId/unarchive`, auth.verify, (req,res) => {

	courseController.unarchiveCourse(req.params.courseId).then(result => res.send(result))
})

// delete
router.delete(`/:courseId/delete`, auth.verify, (req,res) => {

	courseController.deleteCourse(req.params.courseId).then(result => res.send(result))
})

module.exports = router;


