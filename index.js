const express = require("express");
const mongoose = require("mongoose");
const PORT = process.env.PORT || 3000;
const app = express();
const cors = require("cors");

// routes
let userRoutes = require("./routes/userRoutes")
let courseRoutes = require("./routes/courseRoutes")

// middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// mongoose connection
mongoose.connect('mongodb+srv://aidanbigornia:Big0rnia@cluster0.e3n78.mongodb.net/course_booking?retryWrites=true&w=majority',
	{ useNewUrlParser: true, useUnifiedTopology: true }
).then(()=> console.log(`Connected to Database`)).catch((error) => console.log(error));

// schema


// routes

app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes);



app.listen(PORT,() => {console.log(`Server Running on port ${PORT}`)})